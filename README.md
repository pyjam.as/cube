# Cube Etch-a-Sketch

This is a drawing program that you control with a 3x3 rubik's cube.

It is a multi-color(!) etch-a-sketch that can be (only) be controlled with a [GAN smartcube](https://shop.gancube.com/product-category/smart-cubes).

*We do what we must, because we can.*

![normalize sharing scrappy fiddles](./example.png)

## Controls

To change color, rotate the red, green or blue face to change the value of the corresponding color channel. Clockwise increases, and counter-clockwise decreases.

To move your brush around, you use the white and orange faces.
The white face controls the x-axis, and the orange face controls the y-axis.
It kind of makes sense if you hold green in front and white on top.
